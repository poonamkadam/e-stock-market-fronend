import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Company from "./components/CompanyList";
import ViewCompany from "./../src/components/ViewCompany";
import { Container, Row } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './components/Home';
import Header from './components/Header';

const App = () => (
  <Fragment>
    <Header /> 
    <main className="my-5 py-5">
    <Router>
      <Container className="px-0">
        <Row noGutters className="pt-2 pt-md-5 w-100 px-4 px-xl-0 position-relative">
          <Routes>
              <Route exact path="/" element={<Home/>}/>
              <Route exact path="/company-list" element={<Company/>}/>
              <Route exact path="/company" element={<ViewCompany/>}/>
          </Routes> 
        </Row>
      </Container>
      </Router>
    </main>
    
  </Fragment>
);

export default App;
