import { useState, useEffect } from 'react'
import axios from "axios";
import { Link } from "react-router-dom";
import Table from 'react-bootstrap/Table';
import { Modal, Button } from "react-bootstrap";

const Company = () => {
    const [companies, setCompany] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const [name, setName] = useState('');
    const [ceo, setCeo] = useState('');
    const [code, setCode] = useState('');
    const [website, setWebsite] = useState('');
    const [turnover, setTurnover] = useState('');
    const [stockExchange, setStockExchange] = useState('');
    
    const getCompanies = async () => {
        try{
            const apiData = await fetch('/api/v1.0/market/company/getall');
            const response = await apiData.json();
            setCompany(response.data);
        }catch(error) {
            return [];
        }
    }
        
    const deleteCompany = async (code) => {
        try{
            await axios.delete(`/api/v1.0/market/company/delete/${code}`);
            getCompanies();
        }catch(error) {
            return [];
        }
    }
 
    const saveCompany = async (e) => {
        e.preventDefault();
        const response = await axios.post('/api/v1.0/market/company/register',{
            company_code: code,
            company_name: name,
            company_ceo: ceo,
            turnover:turnover, 
            website:website, 
            stock_exchange:stockExchange,
        });
        if(!response.data.data.error){
            setIsOpen(false);
            window.location.reload(false);
        }
        getCompanies();
    }
    useEffect(() => {
        getCompanies();
    }, []);

    return (
        <div>
            <div className="d-flex justify-content-end mt-4 mb-4">
            <Button variant="success" onClick={ () =>  setIsOpen(true) } >Add New</Button>
             </div>
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Comapny Name</th>
                        <th>Comapny Code</th>
                        <th>Comapny CEO</th>
                        <th>Comapny website</th>
                        <th>Comapny Turnover</th>
                        <th>Stock Exchange</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                { companies.map((comapny, index) => (
                        <tr key={ comapny.id }>
                            <td>{ index + 1 }</td>
                            <td>{ comapny.company_name }</td>
                            <td>{ comapny.company_code }</td>
                            <td>{ comapny.company_ceo }</td>
                            <td>{ comapny.website }</td>
                            <td>{ comapny.turnover }</td>
                            <td>{ comapny.stock_exchange }</td>
                            <td>
                             <Link to={`/company?code=${ comapny.company_code}`} className="btn btn-info m-1">View</Link>
                           
                             <Button variant="danger" onClick={ () => deleteCompany(comapny.company_code) } >Delete</Button>
                            </td>
                        </tr>
                    )) }
                </tbody>
           </Table>
           <Modal show={isOpen} onHide={()=>{setIsOpen(false)}}>
                <Modal.Header closeButton>
                    <Modal.Title>Comapany Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <div>
            <form onSubmit={ saveCompany }>
                <div className="row field   pt-3">
                    <label className="label  col-4">Company Name  :- </label>
                    <input 
                        className="input col-7"
                        type="text"
                        placeholder="Company Name"
                        value={ name }
                        onChange={ (e) => setName(e.target.value) }
                        required
                    />
                </div>
                <div className="row field   pt-3">
                    <label className="label  col-4">Comapny Code  :- </label>
                    <input 
                        className="input col-7"
                        type="text"
                        placeholder="Comapny Code "
                        value={ code }
                        onChange={ (e) => setCode(e.target.value) }
                        required
                    />
                </div>
                <div className="row field   pt-3">
                    <label className="label  col-4">Company Ceo  :- </label>
                    <input 
                        className="input col-7"
                        type="text"
                        placeholder="Company Ceo"
                        value={ ceo }
                        onChange={ (e) => setCeo(e.target.value) }
                        required
                    />
                </div>
                <div className="row field   pt-3">
                    <label className="label  col-4">Website  :- </label>
                    <input 
                        className="input col-7"
                        type="text"
                        placeholder="Website"
                        value={ website }
                        onChange={ (e) => setWebsite(e.target.value) }
                        required
                    />
                </div>
                <div className="row field   pt-3">
                    <label className="label  col-4">Turnover  :- </label>
                    <input 
                        className="input col-7"
                        type="text"
                        placeholder="Turnover"
                        value={ turnover }
                        min={9}
                        onChange={ (e) => setTurnover(e.target.value) }
                        required
                    />
                </div>
                <div className="row field   pt-3">
                    <label className="label  col-4">Stock Exchange  :- </label>
                    <input 
                        className="input col-7"
                        type="text"
                        placeholder="Stock Exchange"
                        value={ stockExchange }
                        onChange={ (e) => setStockExchange(e.target.value) }
                        required
                    />
                </div>
                  <div className="row field pt-4">
                     <button className="btn btn-success">Save</button>
                  </div>
              </form>
           </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={()=>{setIsOpen(false)}}>
                    Close
                    </Button>
                </Modal.Footer>
          </Modal>
        </div>
    )
}
 
export default Company