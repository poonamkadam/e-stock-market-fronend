import React, { Fragment } from 'react';
import stock from '../assets/img/stock.jpg';

import {
  Button, UncontrolledAlert, Card, CardImg, CardBody,
  CardTitle, CardSubtitle, CardText
} from 'reactstrap';

const BANNER = 'https://imgur.com/T5mpuQh';

const LeftCard = () => (
  <Fragment>
    <Card>
      <CardImg top width="100%" src={stock} alt="banner" />
      <CardBody>
        <CardTitle className="h3 mb-2 pt-2 font-weight-bold text-secondary">Comapny Deatils </CardTitle>
        <Button color="success" className="font-weight-bold" href="/company-list">Start</Button>
      </CardBody>
    </Card>
    
  </Fragment>
);

export default LeftCard;