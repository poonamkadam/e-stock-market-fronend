import { useState, useEffect } from 'react'
import axios from "axios";
import Table from 'react-bootstrap/Table';
import {useLocation} from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';
import moment from "moment";
import Moment from "react-moment";
import { Link } from "react-router-dom";
import { Modal, Button } from "react-bootstrap";

const ViewCompany = () => {
    const [comapnyData, setCompanyData] = useState();
    const [stockData, setStockData] = useState([]);
    const [priceArray, setPriceArray] = useState([]);
    const [maxStock, setMaxStock] = useState(0);
    const [minStock, setMinStock] = useState(0);
    const [avgStock, setAvgStock] = useState(0);
    const [price, setPrice] = useState('');
    const [isOpen, setIsOpen] = useState(false);
    const [startValue, setStartValue] = useState(moment().format('YYYY-MM-DD'));
    const [endValue, setEndValue] = useState(moment().format('YYYY-MM-DD'));

    const search = useLocation().search;
    const code = new URLSearchParams(search).get('code');

    const getCompanyData = async () => {
      const response = await axios.get(`/api/v1.0/market/company/info/${code}`);
      setCompanyData(response.data.data);
      setStockData(response.data.data.stocks);      
      
      
      const maxValue = Math.max(...response.data.data.stocks.map(stock => stock.stock_price));
      const minValue = Math.min(...response.data.data.stocks.map(stock => stock.stock_price));
      setMaxStock(maxValue); 
      setMinStock(minValue);
   
      let result = response.data.data.stocks.map(a => a.stock_price);
      setPriceArray(result);
      const average = result.reduce((a, b) => parseFloat(a) + parseFloat(b), 0)/ result.length;
      setAvgStock(average);
    }
  

    const onChangeStartDate = e => {
        const newDate = moment(new Date(e.target.value)).format('YYYY-MM-DD');
        setStartValue(newDate);
        setStockData(
            comapnyData.stocks.filter(
                (obj) =>{ return new Date(obj.stock_created_date) >=  new Date(newDate)}
            )
        )
      };

      const onChangeEndDate = e => {
        const newDate = moment(new Date(e.target.value)).format('YYYY-MM-DD');
        setEndValue(newDate);
        setStockData(
            comapnyData.stocks.filter(
                (obj) =>{ return new Date(obj.stock_created_date) <=  new Date(newDate)}
            )
        )
      }
    const addStock = async (e) => {
        e.preventDefault();
        const response = await axios.post(`/api/v1.0/market/stock/add/${code}`,{
            stock_price: price,
            companyId:comapnyData.id,
        });
        if(!response.data.data.error){
            setIsOpen(false);
            window.location.reload(false);
        }
    }
 
    useEffect(() => {
        getCompanyData();
    }, [code]);

    useEffect(() => {
        const maxValue = Math.max(...stockData.map(stock => stock.stock_price));
        const minValue = Math.min(...stockData.map(stock => stock.stock_price)); 
        setMaxStock(maxValue); 
        setMinStock(minValue);
        let result = stockData.map(a => a.stock_price);
        setPriceArray(result);
        const average = result.reduce((a, b) => parseFloat(a) + parseFloat(b), 0)/ result.length;
        setAvgStock(average);

    }, [stockData]);
    
 

    return (
        <Container>
              <div className="d-flex justify-content-between mt-4 mb-4">
               <Link to={`/company-list`} className="btn btn-info m-1  align-items-left text-align-left">Company List</Link>
               <Button variant="success" onClick={ () =>  setIsOpen(true) } >Add Stock</Button>
              </div>
              
             {comapnyData &&
             (<div>
                <span className="d-block pb-4 h2 text-dark border-bottom border-gray  align-items-center">Company : {comapnyData.company_name} </span>
              
                <Row noGutters className="position-relative w-100">
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                      <h6>Comapany Name:- </h6>
                        {comapnyData.company_name}
                    </Col>
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                    <h6>Comapany Code:- </h6>{comapnyData.company_code}
                    </Col>
                </Row>
                <Row noGutters className="position-relative w-100">
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                    <h6>Comapany CEO:-  </h6>{comapnyData.company_ceo}
                    </Col>
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                    <h6>Comapany Website:- </h6>{comapnyData.website}
                    </Col>
                </Row>
                <Row noGutters className="position-relative w-100">
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                    <h6>Comapany Turnover:- </h6>{comapnyData.turnover}
                    </Col>
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                    <h6>Comapany Stock Exchange:- </h6>{comapnyData.stock_exchange}
                    </Col>
                </Row>
                <Container className="mt-5">  
                <Row noGutters className="position-relative w-100">
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                    <strong>Start date:</strong>  <input
                    type="date"
                    value={startValue}
                    onChange={(e)=>onChangeStartDate(e)}
                />
                    </Col>
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right mr-2">
                    <strong>End date:</strong>  <input
                    type="date"
                    value={endValue}
                    onChange={(e)=>onChangeEndDate(e)}
                />  
                    </Col>
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                       <b> Max Stock:-</b> {maxStock}
                    </Col>
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                       <b>Min Stock:-</b> {minStock}
                    </Col>
                    <Col className="d-flex justify-content-xs-start justify-content-lg-right">
                       <b>Avg Stock:- </b> {avgStock}
                    </Col>
                </Row>
             
            
                <Table striped bordered hover size="sm"  className="mt-3">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Stock Price</th>
                            <th>Date</th>
                        </tr>        
                        </thead>
                                <tbody>
                                { stockData.map((stock, index) => (
                                        <tr key={ stock.id }>
                                            <td>{ index + 1 }</td>
                                            <td>{ stock.stock_price }</td>
                                            <td>  <Moment format="DD-MM-YYYY">{stock.stock_created_date}</Moment></td>
                                        </tr>
                                    )) }
                               </tbody>
                    </Table>
           
            </Container>
            </div> )}
            <Modal show={isOpen} onHide={()=>{setIsOpen(false)}}>
                <Modal.Header closeButton>
                    <Modal.Title>Comapany Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <div>
            <form onSubmit={ addStock }>
                <div className="row field   pt-3">
                    <label className="label  col-4">Stock Price  :- </label>
                    <input 
                        className="input col-7"
                        type="text"
                        placeholder="Stock Price"
                        value={ price }
                        onChange={ (e) => setPrice(e.target.value) }
                        required
                    />
                </div>
                  <div className="row field pt-4">
                     <button className="btn btn-success">Save</button>
                  </div>
              </form>
           </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={()=>{setIsOpen(false)}}>
                    Close
                    </Button>
                </Modal.Footer>
          </Modal>
      </Container>
     
    )
}
 
export default ViewCompany