import React, { Component, Fragment } from 'react';
import { Col } from 'reactstrap';
import LeftCard from './LeftCard';

class Home extends Component {

  state = { post: null }
  
  render() {
    return (
        <>
          <Col xs={{ order: 2 }} md={{ size: 4, order: 1 }} tag="aside" className="pb-5 mb-5 pb-md-0 mb-md-0 mx-auto mx-md-0">
            <LeftCard />
          </Col>
      
          <Col xs={{ order: 1 }} md={{ size: 7, offset: 1 }} tag="section" className="py-5 mb-5 py-md-0 mb-md-0">
            <Fragment>
              <div className="position-relative">
                  <span className="d-block pb-4 h2 text-dark border-bottom border-gray">E-Stock Market</span>
                  
                  <article className="pt-5 text-secondary text-justify" style={{ fontSize: '0.9rem', whiteSpace: 'pre-line' }}>
                  E-StockMarket Application is a Restful Microservice application, where it allows users to manage the stocks like create, view stock price details and company details.
                  The core modules of E-Stock Market app are:
                  </article>
                  
                </div> 
            </Fragment>
          </Col>
     </> 
    );
  }
  
}

export default Home;